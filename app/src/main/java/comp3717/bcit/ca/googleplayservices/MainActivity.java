package comp3717.bcit.ca.googleplayservices;


import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import static com.google.android.gms.common.api.GoogleApiClient.Builder;
import static com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import static com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

public class MainActivity
    extends AppCompatActivity
    implements ConnectionCallbacks,
               OnConnectionFailedListener
{
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private static final int LOCATION_ACCESS_CODE = 1;
    private static final String DIALOG_ERROR = "dialog_error";
    private GoogleApiClient googleApiClient;
    private boolean resolvingError;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions();

        if(ContextCompat.checkSelfPermission(this,
                                             Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            createGoogleAPIClient(LocationServices.API);
        }
    }

    private void requestPermissions()
    {
        if(ContextCompat.checkSelfPermission(this,
                                             Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestPermissions(new String[]
                                   {
                                       Manifest.permission.ACCESS_FINE_LOCATION
                                   },
                                   LOCATION_ACCESS_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(final int      requestCode,
                                           final String[] permissions,
                                           final int[]    grantResults)
    {
        switch(requestCode)
        {
            case LOCATION_ACCESS_CODE:
            {
                if(grantResults.length > 0 &&
                   grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    createGoogleAPIClient(LocationServices.API);
                }
            }
        }
    }

    private void createGoogleAPIClient(final Api... apis)
    {
        final Builder builder;

        builder = new Builder(this);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);

        for(final Api api : apis)
        {
            builder.addApi(api);
        }

        googleApiClient = builder.build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(final @Nullable Bundle bundle)
    {
        if(ContextCompat.checkSelfPermission(this,
                                              Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(googleApiClient != null)
            {
                final Location location;

                location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                Toast.makeText(this, location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionSuspended(final int i)
    {
        if(googleApiClient != null)
        {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(final ConnectionResult result)
    {
        if(resolvingError)
        {
            return;
        }

        if(result.hasResolution())
        {
            try
            {
                resolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            }
            catch(final IntentSender.SendIntentException e)
            {
                googleApiClient.connect();
            }
        }
        else
        {
            showErrorDialog(result.getErrorCode());
            resolvingError = true;
        }
    }

    private void showErrorDialog(int errorCode)
    {
        final ErrorDialogFragment dialogFragment;
        final Bundle              args;

        dialogFragment = new ErrorDialogFragment();
        args           = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "errordialog");
    }

    public void onDialogDismissed()
    {
        resolvingError = false;
    }

    @Override
    protected void onActivityResult(final int requestCode,
                                    final int resultCode,
                                    final Intent data)
    {
        if (requestCode == REQUEST_RESOLVE_ERROR)
        {
            resolvingError = false;

            if(resultCode == RESULT_OK)
            {
                if (!googleApiClient.isConnecting() &&
                    !googleApiClient.isConnected())
                {
                    googleApiClient.connect();
                }
            }
        }
    }

    public static class ErrorDialogFragment
        extends DialogFragment
    {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            final int errorCode;

            errorCode = this.getArguments().getInt(DIALOG_ERROR);

            return GoogleApiAvailability.getInstance().getErrorDialog(
                this.getActivity(),
                errorCode,
                REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog)
        {
            final MainActivity mainActivity;

            mainActivity = (MainActivity)getActivity();
            mainActivity.onDialogDismissed();
        }
    }
}
